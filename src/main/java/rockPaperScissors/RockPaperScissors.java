package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
  public static void main(String[] args) {  
    
    Scanner scanner = new Scanner(System.in);
    

    int round = 1;
    int playerScore = 0;
    int computerScore = 0;



    while (true) {
      String[] rps = {"rock", "paper", "scissors"};
      String computerMove = rps[new Random().nextInt(rps.length)];

      String playerMove;
      
    System.out.println("Let's play round " + round);

      while(true) {
        System.out.println("Your choice (Rock/Paper/Scissors)?");
        playerMove = scanner.nextLine();
        if (playerMove.equals("rock") || playerMove.equals("paper") || playerMove.equals("scissors")) {
          break;
        }
        System.out.println(playerMove + " isn't a valid move, please write rock,paper or scissors.");
      }
      
      
      if (playerMove.equals(computerMove)) {
        System.out.println("Human chose " + playerMove+ ", " + "computer chose "+ computerMove	+ "."+" It's a tie!");
        System.out.println("Score: human " + playerScore + ", computer " + computerScore );
      }
      else if (playerMove.equals("rock")) {
        if (computerMove.equals("paper")) {
          System.out.println("Human chose " + playerMove+ ", " + "computer chose "+ computerMove	+ "."+" Computer wins!");
          computerScore++;
          System.out.println("Score: human " + playerScore + ", computer " + computerScore );
          
        } else if (computerMove.equals("scissors")) {
          System.out.println("Human chose " + playerMove+ ", " + "computer chose "+ computerMove	+ "."+" Human wins!");
          playerScore++;
          System.out.println("Score: human " + playerScore + ", computer " + computerScore );
        }
      }
      
      else if (playerMove.equals("paper")) {
        if (computerMove.equals("rock")) {
          System.out.println("Human chose " + playerMove+ ", " + "computer chose "+ computerMove	+ "."+" Human wins!");
          playerScore++;
          System.out.println("Score: human " + playerScore + ", computer " + computerScore );
          
          
        } else if (computerMove.equals("scissors")) {
          System.out.println("Human chose " + playerMove+ ", " + "computer chose "+ computerMove	+ "."+" Computer wins!");
          computerScore++;
          System.out.println("Score: human " + playerScore + ", computer " + computerScore );
        }
      }
      
      else if (playerMove.equals("scissors")) {
        if (computerMove.equals("paper")) {
          System.out.println("Human chose " + playerMove+ ", " + "computer chose "+ computerMove	+ "."+" Human wins!");
          playerScore++;
          System.out.println("Score: human " + playerScore + ", computer " + computerScore );
          
        } else if (computerMove.equals("rock")) {
          System.out.println("Human chose " + playerMove+ ", " + "computer chose "+ computerMove	+ "."+" Computer wins!");
          computerScore++;
          System.out.println("Score: human " + playerScore + ", computer " + computerScore );
        }
      }
      
      System.out.println("Do you wish to continue playing? (y/n)?");
      String playAgain = scanner.nextLine();
      

      if (!playAgain.equals("y")) {
          System.out.println("Bye bye :)");
        break;
      } 
      round++;
    }
    scanner.close();
  }
}
